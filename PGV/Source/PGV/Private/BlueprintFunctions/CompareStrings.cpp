// Fill out your copyright notice in the Description page of Project Settings.


#include "CompareStrings.h"

bool UCompareStrings::SGreaterThan(FString StringA, FString StringB)
{
        return (FCString::Strcmp(*StringA, *StringB) > 0);
}


bool UCompareStrings::SLessThan(FString StringA, FString StringB)
{
        return (FCString::Strcmp(*StringA, *StringB) < 0);
}
