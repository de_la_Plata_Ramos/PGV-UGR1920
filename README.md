# PGV-Práctica

Proyecto realizado para la asignatura "Programación Gráfica de Videojuegos".

## Notas de la versión

Para descargar el proyecto correctamente con LFS es necesario usar git clone. 
Descargar el código fuente desde un zip o fichero comprimido no funcionará.

Hemos decidido que le desarrollo del proyecto continúe más allá de la fecha de 
entrega de la asignatura, por lo que clonar el proyecto nos llevará al último 
commit de la rama predeterminada, master.

Para observar el estado del proyecto en el momento de de la entrega, tenemos que 
volver atrás en el historial de commits. Para ello proporcionamos el hash del 
último commit antes de la entrega: 86be0ffb

Una vez clonado el proyecto, podemos restaurar el repositorio a la versión de entrega
con el siguiente comando:

*git checkout 86be0ffb*

## Notas del proyecto

*  Hay 2 mapas jugables, los cuales se encuentran dentro de la carpeta /Content/Maps
    * Map_1 y Map_2  